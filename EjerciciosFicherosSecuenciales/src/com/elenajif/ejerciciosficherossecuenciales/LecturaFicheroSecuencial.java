package com.elenajif.ejerciciosficherossecuenciales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LecturaFicheroSecuencial {
	// cualquier flujo de informaci�n en Java necesita un Stream (flujo)
	// flujo conexi�n entre el programa y el dispositivo de entrada o salida
	// Tipos de flujos
	// caracteres (texto)
	// bytes (binarios)
	// Tipos archivos
	// texto (no enriquecidos), abrimos con bloc notas o wordpad
	// binarios (enriquecido), video, texto con im�genes...
	// clases bytes
	// Reader, Writer
	// clases caracteres
	// InputStreamReader, OutputStreamReader
	// modos de acceso (secuencial, aleatorio)
	// modos de acceso (lectura, BufferedReader) (escritura, PrintWriter)

	public static void main(String[] args) {
		try {
			// 1.- Crear el puntero al archivo
			BufferedReader f = new BufferedReader(new FileReader("datos.txt"));
			// ruta -> "c:\\documentos\\datos.txt" -> necesario \\
			// secuencia escape \n

			// 2.- recorrer el archivo linea a linea
			String nombre = "";
			nombre = f.readLine();
			while (nombre != null) {
				System.out.println(nombre);
				nombre = f.readLine();
			}
			
			// el proceso de leer un fichero secuencialmente
			// finaliza cuando se lee el fin del fichero (EOF)
			// Al final del fichero encuentra [null]
			
			// 3.- cerrar el archivo
			f.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
		} catch (IOException e) {
			System.out.println("Error, el archivo no es accesible");
		} 
	}

}

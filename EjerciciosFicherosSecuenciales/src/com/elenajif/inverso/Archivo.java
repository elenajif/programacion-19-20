package com.elenajif.inverso;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Archivo {

	// 1.- Atributos (nombreArchivo)
	String nombreArchivo;

	// 2.- Constructores
	public Archivo() {
		this.nombreArchivo = "";
	}

	public Archivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	// 3.- m�todo visualizarArchivo
	public static void visualizarArchivo(String nombreArchivo) {
		try {
			// 1.- abro para lectura
			BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));

			// 2.- recorro linea a linea hasta null
			String linea = "";
			linea = origen.readLine();
			while (linea != null) {
				System.out.println(linea);
				linea = origen.readLine();
			}

			// 3.- cierro archivo
			origen.close();
		} catch (FileNotFoundException e) {
			System.out.println("El fichero no existe");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Archivo inaccesible");
			System.exit(0);
		}
	}

	// 4.- m�todo invertirArchivo
	public void crearArchivoInvertido() throws IOException {
		// 1.- Abrir el archivo para leer y crear el vector (ArrayList)
		BufferedReader origen = new BufferedReader(new FileReader(nombreArchivo));
		ArrayList<String> v = new ArrayList<String>();

		// 2.- Leo el archivo y escribo en el vector
		String linea = "";
		linea = origen.readLine();
		while (linea != null) {
			v.add(linea);
			linea = origen.readLine();
		}

		// 3.- Cerrar el archivo
		origen.close();

		// 4.- Abro el segundo archivo para escribir
		PrintWriter destino = new PrintWriter(new FileWriter("nombreInvertido.txt", false));

		// 5.- Leo del vector del rev�s y voy escribiendo
		for (int i = v.size() - 1; i >= 0; i--) {
			destino.println(v.get(i));
		}

		// 6.- Cerrar el archivo
		destino.close();

	}
}

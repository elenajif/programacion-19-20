package com.elenajif.principal;

import com.elenajif.claseenumerados.Transporte;

public class Enumerados {
    public static void main(String[] args) {
        Transporte tp;
        tp=Transporte.AVION;
        System.out.println("Valor inicial del ENUM transporte: "+tp);
        System.out.println();
        System.out.println("Asigno nuevo valor al ENUM");
        tp=Transporte.TREN;
        //Comparaci�n de 2 valores enum
        if (tp==Transporte.TREN)
            System.out.println("ENUM transporte tiene el valor de TREN");
        //enum para controlar sentencia switch
        switch(tp){
            case COCHE:
                System.out.println("Un auto lleva personas.");
                break;
            case CAMION:
                System.out.println("Un cami�n lleva carga.");
                break;
            case AVION:
                System.out.println("Un avi�n vuela.");
                break;
            case TREN:
                System.out.println("Un tren corre sobre rieles.");
                 break;
            case BARCO:
                System.out.println("Un barco navega en el agua.");
                break;
        }
    }
}

package com.elenajif.ejerciciosvectores;

import java.util.Scanner;

public class Ejercicio1Vectores {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Dame la longitud del vector");
		int tam=entrada.nextInt();
		int vectorcillo[]=new int[tam];
		rellenarArray(vectorcillo);
		mostrarArray(vectorcillo);
		entrada.close();
	}

	public static void rellenarArray(int[] vector) {
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < vector.length; i++) {
			System.out.println("Introduce un n�mero");
			vector[i] = input.nextInt();
		}
		input.close();
	}

	public static void mostrarArray(int[] vector) {
		for (int i = 0; i < vector.length; i++) {
			System.out.println("En el indice " + i + " est� el valor " + vector[i]);
		}
	}
}

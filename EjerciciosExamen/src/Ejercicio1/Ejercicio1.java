package Ejercicio1;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int opcion=0;
		do {
			System.out.println("Men� de opciones");
			System.out.println("1.- Contar caracteres");
			System.out.println("2.- Contrase�a correcta");
			System.out.println("3.- Salir");
			System.out.println("Elegir opci�n");
			opcion=input.nextInt();
			input.nextLine();
			
			switch (opcion) {
			case 1:
				System.out.println("Introduce un caracter");
				char caracter=input.nextLine().charAt(0);
				System.out.println("Introduce una cadena");
				String cadena=input.nextLine();
				int contador=0;
				for (int i=0;i<cadena.length();i++) {
					if (cadena.charAt(i)==caracter) {
						contador++;
					}
				}
				System.out.println("El n�mero de veces que aparece el caracter es: "+contador);
				break;
			case 2:
				System.out.println("Dame una contrase�a");
				String password=input.nextLine().toLowerCase();
				boolean vocales=false;
				boolean letras=false;
				for (int i=0;i<password.length();i++) {
					if (password.charAt(i)=='a' || password.charAt(i)=='e' || password.charAt(i)=='i' || password.charAt(i)=='o' || password.charAt(i)=='u') {
						vocales=true;
					}
					if (password.charAt(i)>='a' && password.charAt(i)<='z') {
						letras=true;
					}
				}
				if (vocales==true && letras==true) {
					System.out.println("Contrase�a correcta");
				} 
				else {
					System.out.println("Contrase�a incorrecta");
				}
				break;
			case 3:
				System.out.println("Salir");
				break;	
			default:
				System.out.println("Opci�n incorrecta");
				break;
			}
			
		} while (opcion!=3);
		input.close();
	}

}

public class Ejercicio5 {
       public static void main(String args[]) { 
        Scanner sc=new Scanner(System.in);  
        System.out.println("Dame un numero entero, mayor que 0");
        int numero=sc.nextInt();
        int var = 0;
            while (numero >= 0){
                var = numero + var;
                numero--;
            }
        System.out.println("El sumatorio es " + var);
        }
}
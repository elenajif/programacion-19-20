package com.elenajif.ficherosrafcadenas;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class ListaNombres {
	String archivo;

	public ListaNombres(String nombre) {
		this.archivo = nombre;
	}

	// int 4 bytes
	// double 8 bytes
	// cadenas longitud + 2 bytes
	public void rellenarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.print("Nombre = ");
				String nombre = in.readLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	private String formatearNombre(String nombre, int lon) {
		// convertir la cadena al tama�o indicado
		if (nombre.length() > lon) {
			// recorta
			return nombre.substring(0, lon);
		} else {
			// completar con espacios en blanco
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
			return nombre;
		}
	}

	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("Fin fichero ");
					finFichero = true;
					f.close();
				}
			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	public void modificarArchivo() {
		try {
			// 1.- pido el nombre a buscar y el nuevo valor
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String oldName = "Elena";
			String newName = "Elena Jimenez";
			System.out.println("Nombre a modificar");
			oldName = in.readLine();
			System.out.println("Dame el nuevo nombre");
			newName = in.readLine();
			// 2.- abro el archivo
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			// 3.- busco en el archivo y cuando lo encuentro lo modifico
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldName)) {
						// tengo que modificarlo
						f.seek(f.getFilePointer() - 22);
						newName = formatearNombre(newName, 20);
						f.writeUTF(newName);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no est� en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}

		} catch (IOException e) {
			System.out.println("error");
		}
	}
}

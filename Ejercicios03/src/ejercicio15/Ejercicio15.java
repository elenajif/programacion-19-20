package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una hora en formato horas:minutos:segundos");
		String horaCompleta = input.nextLine();

		// Obtener las posiciones de los dos separados
		int posicionPrimerosDosPuntos = horaCompleta.indexOf(":");
		int posicionSegundosDosPuntos = horaCompleta.lastIndexOf(":");
		// Otra posibilidad
		// int posicionSegundosDosPuntos = horaCompleta.indexOf(":",
		// posicionPrimerosDosPuntos);

		// Obtengo las subcadenas con cada parte de la hora
		String horasCadena = horaCompleta.substring(0, posicionPrimerosDosPuntos);
		String minutosCadena = horaCompleta.substring(posicionPrimerosDosPuntos + 1, posicionSegundosDosPuntos);
		String segundosCadena = horaCompleta.substring(posicionSegundosDosPuntos + 1);

		// Convierto cada String a int
		int horas = Integer.parseInt(horasCadena);
		int minutos = Integer.parseInt(minutosCadena);
		int segundos = Integer.parseInt(segundosCadena);

		if (horas >= 0 && horas <= 23 && minutos >= 0 && minutos <= 59 && segundos >= 0 && segundos <= 59) {
			System.out.println("la hora esta correcta");
		} else {
			System.out.println("La hora es incorrecta");
		}

		System.out.println("Introduce una fecha en formato dia/mes/anno");
		String fechaCompleta = input.nextLine();

		int posicionPrimeraBarra = fechaCompleta.indexOf('/');
		int posicionSegundaBarra = fechaCompleta.lastIndexOf('/');

		String cadenaDia = fechaCompleta.substring(0, posicionPrimeraBarra);
		String cadenaMes = fechaCompleta.substring(posicionPrimeraBarra + 1, posicionSegundaBarra);
		String cadenaAnno = fechaCompleta.substring(posicionSegundaBarra + 1);

		int dia = Integer.parseInt(cadenaDia);
		int mes = Integer.parseInt(cadenaMes);
		int anno = Integer.parseInt(cadenaAnno);

		if (dia >= 1 && dia <= 30) {
			if (mes >= 1 && mes <= 12) {
				if (anno >= 1) {
					System.out.println("Fecha correcta");
				} else {
					System.out.println("A�o incorrecto");
				}
			} else {
				System.out.println("Mes incorrecto");
			}
		} else {
			System.out.println("Dia incorrecto");
		}

		input.close();
	}

}

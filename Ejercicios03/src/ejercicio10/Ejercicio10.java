package ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero (1)");
		int numero1 = input.nextInt();
		
		System.out.println("Introduce un numero (2)");
		int numero2 = input.nextInt();
		
		System.out.println("Introduce un numero (3)");
		int numero3 = input.nextInt();
		
		if(numero1 > numero2) {
			if(numero1 > numero3) {
				System.out.println(numero1 + " es el mayor");
			} else {
				System.out.println(numero3 + " es el mayor");
			}
		} else {
			if(numero2 > numero3) {
				System.out.println(numero2 + " es el mayor");
			} else {
				System.out.println(numero3 + " es el mayor");
			}
		}
		
		input.close();
	}
}

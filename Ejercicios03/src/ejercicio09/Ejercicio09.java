package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);

		System.out.println("Introduce un entero");
		int numero1 = lector.nextInt();

		System.out.println("Introduce otro entero");
		int numero2 = lector.nextInt();
		
		if( numero1 > numero2 ) {
			System.out.println(numero1 + " es mayor");
			
		} else if(numero1 == numero2) {
			System.out.println("son iguales");
			
		}else {
			System.out.println(numero2 + " es mayor");
		}
		
		lector.close();
	}

}

package com.elenajif.cuentasbancarias;

public class CuentaAhorroFija extends Cuenta{

	//constructores
	public CuentaAhorroFija() {
		super();
		this.interes=2.6;
	}
	
	public CuentaAhorroFija(String numero, String titular, double saldo, double interes) {
		super(numero, titular,saldo, interes);
		this.interes=interes;
	}
	
	//metodos
	public double ingresoMes() {
		saldo+=100;
		return saldo;
	}
}

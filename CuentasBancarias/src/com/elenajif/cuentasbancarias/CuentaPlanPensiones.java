package com.elenajif.cuentasbancarias;

public class CuentaPlanPensiones extends Cuenta{
	// atributos
	double cotizacion;
	
	//constructores
	public CuentaPlanPensiones() {
		super();
		this.interes=3.22;
		this.cotizacion=6.5;
	}
	
	//setter y getter
	public double getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(double cotizacion) {
		this.cotizacion = cotizacion;
	}

}

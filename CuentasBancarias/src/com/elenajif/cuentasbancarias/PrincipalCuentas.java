package com.elenajif.cuentasbancarias;

public class PrincipalCuentas {

	public static void main(String[] args) {
		System.out.println("Cuenta constructor sin parámetros");
		Cuenta cuenta01 = new Cuenta();
		System.out.println("Numero "+cuenta01.getNumero());
		System.out.println("Titular "+cuenta01.getTitular());
		System.out.println("Saldo "+cuenta01.getSaldo());
		System.out.println("Interes "+cuenta01.getInteres());
		System.out.println("Ingreso "+cuenta01.ingreso(50));
		System.out.println("Cuenta constructor con parámetros");
		Cuenta cuenta02 = new Cuenta("11111","Titular1",222,0.2);
		System.out.println("Numero "+cuenta02.getNumero());
		System.out.println("Titular "+cuenta02.getTitular());
		System.out.println("Saldo "+cuenta02.getSaldo());
		System.out.println("Interes "+cuenta02.getInteres());
		System.out.println("Ingreso "+cuenta02.ingreso(150));
		
		System.out.println("CuentaAhorroFija constructor sin parámetros");
		CuentaAhorroFija cuenta03 = new CuentaAhorroFija();
		System.out.println("Numero "+cuenta03.getNumero());
		System.out.println("Titular "+cuenta03.getTitular());
		System.out.println("Saldo "+cuenta03.getSaldo());
		System.out.println("Interes "+cuenta03.getInteres());
		System.out.println("Ingreso "+cuenta03.ingreso(1000));
		System.out.println("Ingreso mes "+cuenta03.ingresoMes());
		
		System.out.println("Cuenta constructor con parámetros");
		CuentaAhorroFija cuenta04 = new CuentaAhorroFija("11111","Titular1",222,0.2);
		System.out.println("Numero "+cuenta04.getNumero());
		System.out.println("Titular "+cuenta04.getTitular());
		System.out.println("Saldo "+cuenta04.getSaldo());
		System.out.println("Interes "+cuenta04.getInteres());
		System.out.println("Ingreso "+cuenta04.ingreso(500));
		System.out.println("Ingreso mes "+cuenta04.ingresoMes());

	}

}

package ejercicios094;

public class Libro {

	String titulo;
	String autor;
	int anyo;
	String editorial;
	float precio;

	public Libro() {

	}

	public Libro(String titulo, String autor, int anyo, String editorial, float precio) {
		this.titulo = titulo;
		this.autor = autor;
		this.anyo = anyo;
		this.editorial = editorial;
		this.precio = precio;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	static float PrecioConIva(float precio,float iva ){
		return (precio + (precio *iva/100)) ;
		}

}

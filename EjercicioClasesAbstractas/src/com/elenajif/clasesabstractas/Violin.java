package com.elenajif.clasesabstractas;

/**
 * Clase Concreta Violin, hija de Instrumento
 */
public class Violin extends Instrumento {

	public Violin() {
		tipo = "Violin";
	}

	public void tocar() {
		System.out.println("Tocar El violin");
	}
}

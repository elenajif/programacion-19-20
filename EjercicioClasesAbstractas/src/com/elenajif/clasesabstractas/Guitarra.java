package com.elenajif.clasesabstractas;

/**
 * Clase Concreta Guitarra, hija de Instrumento
 */
public class Guitarra extends Instrumento {

	public Guitarra() {
		tipo = "Guitarra";
	}

	public void tocar() {
		System.out.println("Tocar La Guitarra");
	}
}

package com.elenajif.clasesabstractas;

/**
 * Clase Concreta Saxofon, hija de Instrumento
 */
public class Saxofon extends Instrumento {

	public Saxofon() {
		tipo = "Saxofon";
	}

	public void tocar() {
		System.out.println("Tocar el Saxofon");
	}
}

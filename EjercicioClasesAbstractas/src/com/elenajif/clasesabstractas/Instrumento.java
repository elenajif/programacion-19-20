package com.elenajif.clasesabstractas;

// la abstracci�n permite resaltar lo mas representativo de algo sin importar los detalles
// una clase Abstracta es similar a una clase normal salvo que al menos uno de sus m�todos debe ser abstracto
// su uso depende de la aplicaci�n del concepto de Herencia
// a�adiremos la palabra reservada abstract

// Una clase Abstracta No puede ser instanciada (new), solo puede ser heredada. 
// Si al menos un m�todo de la clase es abstract, la clase completa sea definida abstract
// La clase puede tener el resto de m�todos no abstractos.
// Los m�todos abstract no llevan cuerpo (no llevan los caracteres {}). 
// La subclase que herede de una clase abstract debe implementar TODOS los m�todos de la superclase.

// Al trabajar clases y m�todos abstractos, mantenemos la aplicaci�n mas organizada
// Al no poder instanciarla aseguramos que las propiedades solo est�n disponibles para sus clases hijas

// Con las Clases Abstractas lo que hacemos es definir un proceso general que luego sera implementado 
// Sera implementado por las clases concretas que hereden dichas funcionalidades

// Si tengo una clase que hereda de otra Abstracta, estoy obligado a poner en el c�digo
// Pero esta vez ser�n m�todos concretos con funcionalidad dependiendo de para que se necesite

// clase Abstracta Instrumento, propiedad tipo y un m�todo abstracto tocar()
// clases hijas Guitarra, Saxofon y Violin con las propiedades de la clase Padre
// Todos los instrumentos musicales se pueden tocar
// Creamos el m�todo abstracto tocar, es com�n a todos los instrumentos sin importar como se tocan
// Al heredar de la clase Instrumento, las clases hijas DEBEN implementar este m�todo y darle funcionalidad

/**
 * Clase Abstracta Instrumento
 */
public abstract class Instrumento {

	public String tipo;

	public abstract void tocar();
}

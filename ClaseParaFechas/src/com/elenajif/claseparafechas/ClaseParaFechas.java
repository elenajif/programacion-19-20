package com.elenajif.claseparafechas;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;

public class ClaseParaFechas {

	public static void main(String[] args) {
		System.out.println("Obtener la fecha de hoy (basada en el reloj del Sistema");
		System.out.println("Creamos una instancia de clase LocalDate");
		LocalDate fechaActual = LocalDate.now();
		System.out.println("fechaActual "+ fechaActual);

		System.out.println("");
		System.out.println("Obtener una fecha cualquiera");
		LocalDate fecha = LocalDate.of(2018, 3, 23);
		System.out.println("fecha "+fecha);

		System.out.println("");
		System.out.println("Para obtener una fecha a partir de un String (formato aaaa-mm-dd)");
		LocalDate fecha1 = LocalDate.parse("2007-12-03");
		System.out.println("fecha1 "+fecha1);
		
		System.out.println("");
		System.out.println("fechaActual "+ fechaActual);
		System.out.println("M�todos de instancia");
		System.out.print("toString fechaActual ");
		System.out.println(fechaActual.toString());
		System.out.print("compareTo fecha con fechaActual ");
		System.out.println(fechaActual.compareTo(fecha));
		System.out.print("isBefore fecha ");
		System.out.println(fechaActual.isBefore(fecha));
		System.out.print("isAfter fecha ");
		System.out.println(fechaActual.isAfter(fecha));
		System.out.print("getYear fechaActual ");
		System.out.println(fechaActual.getYear());
		System.out.print("getMonthValue fechaActual ");
		System.out.println(fechaActual.getMonthValue());
		System.out.print("getDayOfMonth fechaActual ");
		System.out.println(fechaActual.getDayOfMonth());
		System.out.print("getDayOfWeek fechaActual ");
		System.out.println(fechaActual.getDayOfWeek());
		System.out.print("minusDay fechaActual minusDays(30) ");
		System.out.println(fechaActual = fechaActual.minusDays(30));
		System.out.print("minusMonths fechaActual minusMonths(4) ");
		System.out.println(fechaActual.minusMonths(4));
		System.out.print("minusYears fechaActual minusYears(5) ");
		System.out.println(fechaActual.minusYears(5));
		System.out.print("plusDays fechaActual plusDays(30) ");
		System.out.println(fechaActual.plusDays(30));
		System.out.print("plusMonths fechaActual plusMonths(4) ");
		System.out.println(fechaActual.plusMonths(4));
		System.out.print("plusYears fechaActual plusYears(5) ");
		System.out.println(fechaActual.plusYears(5));
		System.out.print("withDayOfMonth fechaActual withDayOfMonth(7) ");
		System.out.println(fechaActual.withDayOfMonth(7));
		System.out.print("withMonth fechaActual withMonth(3) ");
		System.out.println(fechaActual.withMonth(3));
		System.out.print("withYear fechaActual withYear(2015) ");
		System.out.println(fechaActual.withYear(2015));

		System.out.println("");
		System.out.println("El objeto LocalDate no cambia nunca, el nuevo objeto generado hay que guardarlo en una variable");
		System.out.println("FechaActual antes "+fechaActual);
		fechaActual = fechaActual.plusDays(234);
		System.out.println("FechaActual despues "+fechaActual);
		
		System.out.println("");
		System.out.println("Clase Period");
		LocalDate fecha2 = LocalDate.now();
		LocalDate fecha3 = LocalDate.parse("2000-04-01");
		Period diferencia = Period.between(fecha2, fecha3);
		System.out.print("Muestra los d�as de diferencia ");
		System.out.println(diferencia.getDays());
		System.out.print("Muestra los meses de diferencia ");
		System.out.println(diferencia.getMonths());
		System.out.print("Muestra los a�os de diferencia ");
		System.out.println(diferencia.getYears());
		
		System.out.println("");
		System.out.println("Clase LocalDateTime");
		LocalDateTime fechaHora = LocalDateTime.now();
		System.out.println("fechaHora "+fechaHora);
		LocalDateTime dateTime = LocalDateTime.of(2016, 7, 25, 22, 11, 30);
		System.out.println("dateTime LocalDateTime.of(2016, 7, 25, 22, 11, 30) "+dateTime);
		LocalDateTime dateTime1 = dateTime.withYear(2017);
		System.out.println("dateTime1 dateTime.withYear(2017) "+dateTime1);
		LocalDateTime dateTime2 = dateTime.withMonth(8);
		System.out.println("dateTime2 dateTime.withMonth(8) "+dateTime2);
		LocalDateTime dateTime3 = dateTime.withDayOfMonth(27);
		System.out.println("dateTime3 dateTime.withDayOfMonth(27) "+dateTime3);
		LocalDateTime dateTime4 = dateTime.withHour(20);
		System.out.println("dateTime4 dateTime.withHour(20) "+dateTime4);
		LocalDateTime dateTime5 = dateTime.withMinute(25);
		System.out.println("dateTime5 dateTime.withMinute(25) "+dateTime5);
		LocalDateTime dateTime6 = dateTime.withSecond(23);
		System.out.println("dateTime6 dateTime.withSecond(23)"+dateTime6);
		LocalDateTime dateTime7 = dateTime.withNano(24);
		System.out.println("dateTime7 dateTime.withNano(24) "+dateTime7);
		
		System.out.println("");
		System.out.println("Clase LocalTime");
		LocalTime horaActual = LocalTime.now();
		System.out.println("horaActual "+horaActual);
		System.out.println("Almacenar una hora cualquiera (15:45:59)");
		LocalTime hora = LocalTime.of(15, 45, 59);
		System.out.println("hora "+hora);
		System.out.println("Almacenar una hora a partir de un String");
		LocalTime hora1 = LocalTime.parse("23:12:59");
		System.out.println("hora1 "+hora1);
		System.out.println("Siempre en formato hh-mm-ss 14:30:35");
		LocalTime time = LocalTime.of(14, 30, 35);
		System.out.println("time LocalTime.of(14, 30, 35) "+time);
		LocalTime time1 = time.withHour(20);
		System.out.println("time1 time.withHour(20) "+time1);
		LocalTime time2 = time.withMinute(25);
		System.out.println("time2 withMinute(25) "+time2);
		LocalTime time3 = time.withSecond(23);
		System.out.println("time3 withSecond(23) "+time3);
		LocalTime time4 = time.withNano(24);
		System.out.println("time4 time.withNano(24) "+time4);
		LocalTime time5 = time.with(ChronoField.HOUR_OF_DAY, 23);
		System.out.println("time5 time.with(ChronoField.HOUR_OF_DAY, 23) "+time5);

		System.out.println("");
		System.out.println("DateTimeFormatter");
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern("d/M/y");
		LocalDate fecha4 = LocalDate.now();
		System.out.println("fecha4 "+fecha4);

		System.out.println("");
		System.out.println("Muestra la fecha en formato d�a/mes/a�o");
		System.out.println("fecha.format(formateador) "+fecha.format(formateador));
		System.out.println("formateador.format(fecha) "+formateador.format(fecha));
	}
}
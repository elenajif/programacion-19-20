package com.elenajif.ejerciciospruebaexcepciones;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Ejercicio5ConExcepciones {

	public static void main(String[] args) {
		try {
			Scanner input = new Scanner(System.in);
			System.out.println("Dame un n�mero");
			double x = input.nextDouble();
			System.out.println("Raiz cuadrada de " + x + " = " + Math.sqrt(x));
			input.close();
		} catch (InputMismatchException e) {
			System.out.println("Error entrada de datos");
		}

	}

}

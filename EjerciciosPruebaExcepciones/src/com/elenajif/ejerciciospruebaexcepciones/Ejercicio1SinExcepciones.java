package com.elenajif.ejerciciospruebaexcepciones;

public class Ejercicio1SinExcepciones {

	public static void main(String[] args) {
		// checked obligatorio controlarlas
		// unchecked no obligatorio controlarlas
		// todas derivan de Exception
		int i;
		int valor;
		i = 3;
		valor = i / 0;
		System.out.println(valor);

	}

}

package com.elenajif.ejerciciospruebaexcepciones;

public class Ejercicio9ConExcepciones {

	public static void main(String[] args)  {
		try {
			comprobarDiaMes(35);
		} catch (Exception e) {
			System.out.println("Mensaje de error "+e.getMessage());
		}
	}

	public static void comprobarDiaMes(int dia) throws Exception {
		if (dia > 31 || dia < 1) {
				Exception excepcion = new Exception("N�mero de dia fuera de rango");
				throw excepcion;
		}
	}


}

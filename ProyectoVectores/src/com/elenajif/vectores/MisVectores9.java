package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores9 {
	
	public static void main (String[] args) {
		int[] miVectorcillo=insertarDatosArray();
		sumarArray(miVectorcillo);
		maxArray(miVectorcillo);
		promedioArray(miVectorcillo);
	}
	
	public static int[] insertarDatosArray() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Dame la longitud del vector");
		int tam=scanner.nextInt();
		int[] array = new int[tam];
		System.out.println("Insertar datos");
		for (int i=0;i<array.length;i++) {
			System.out.println("Dame la componente "+i+ " del vector");
			array[i]=scanner.nextInt();
		}
		scanner.close();
		return array;
	}
	
	public static void sumarArray(int[] array) {
		System.out.println("Sumar Array");
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		System.out.println("La suma es: " + suma);
	}
	
	public static void maxArray(int[] array) {
		System.out.println("M�ximo Array");
		int maximo = array[0];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maximo)
				maximo = array[i];
		}
		System.out.println("El maximo es: " + maximo);
	}
	
	public static void promedioArray(int[] array) {
		System.out.println("Promedio Array");
		int media = 0;
		int suma = 0;
		for (int i = 0; i < array.length; i++) {
			suma += array[i];
		}
		media = suma / array.length;
		System.out.println("La media es: " + media);
	}
}

package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores3 {

	public static void main(String[] args) {
		// declaramos un vector
		Double miVector[] = new Double[5];
		//declaramos un scanner
		Scanner lectura = new Scanner(System.in);
		//pedimos los datos
		for (int i=0;i<5;i++) {
			System.out.println("Dame la componente "+i+" del vector");
			miVector[i]=lectura.nextDouble();
		}
		//mostrar los datos
		for (int i=0;i<miVector.length;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector[i]);
		}
		//calculamos la media
		double suma=0;
		double media=0;
		for (int i=0;i<miVector.length;i++) {
			suma+=miVector[i];
				}
		media=suma/miVector.length;
		System.out.println("La media es "+media);
		lectura.close();
	}

}

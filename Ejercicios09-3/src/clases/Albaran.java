package clases;

import java.time.LocalDate;
/**
 * Clase en la que se guarda la informacion de distintos albaranes
 * @author DAW
 *
 */
public class Albaran {
	
	//atributos
	private String codAlbaran;
	private double precio;
	private String codVehiculo;
	private LocalDate fecha;
	
	/**
	 * Costructor
	 * genera un nuevo albaran dado un codAlbaran
	 * @param codAlbaran es el identificador que tendra el albaran
	 */

	public Albaran(String codAlbaran) {
		this.codAlbaran = codAlbaran;
	}
	
	//setter y getter
	
	public String getCodAlbaran() {
		return codAlbaran;
	}
	public void setCodAlbaran(String codAlbaran) {
		this.codAlbaran = codAlbaran;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getCodVehiculo() {
		return codVehiculo;
	}
	public void setCodVehiculo(String codVehiculo) {
		this.codVehiculo = codVehiculo;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	//metodo toString
	@Override
	public String toString(){
		return codAlbaran + " " + precio + " " + codVehiculo + " " + fecha;
	}
	
	
}

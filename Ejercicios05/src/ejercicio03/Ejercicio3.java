package ejercicio03;

import java.util.Scanner;

public class Ejercicio3 {

	static final String MENSAJE_FIN = "Fin de programa";
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce un numero mayor a 1");
		
		int numero = input.nextInt();
		
		int contador = numero;
		
		//Mostrandolos en lineas distintas
		do{
			System.out.println(contador);
			contador--;
		}while(contador >= 1);
		
		//Mostrandolos en la misma linea
		contador = 100;
		do{
			System.out.print(contador);
			contador--;
		}while(contador >= 1);
		
		System.out.println("\n" + MENSAJE_FIN);
		
		input.close();
	}

}

package ejercicio06;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args){	
		Scanner input = new Scanner(System.in);
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		
		System.out.println("Rellenamos el primer array");
		for(int i = 0; i < array1.length; i++){
			System.out.println("Introduce un entero");
			array1[i] = input.nextInt();
			
		}
		
		System.out.println("Rellenamos el segundo array");
		for(int i = 0; i < array2.length; i++){
			System.out.println("Introduce un entero");
			array2[i] = input.nextInt();
		}
		
		int[] arraySuma = sumarArrays(array1, array2);
		
		for(int i = 0; i < array2.length; i++){
			System.out.print(arraySuma[i] + " ");
		}
		
		input.close();
	}

	private static int[] sumarArrays(int[] array1, int[] array2) {
		int[] arraySuma = new int[10];
		//Sumo las celdas de los dos arrays
		for(int i = 0; i < arraySuma.length; i++){
			arraySuma[i] = array1[i] + array2[i];
		}
		return arraySuma;
	}
}

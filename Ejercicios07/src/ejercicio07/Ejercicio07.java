package ejercicio07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//Pido el tamano del array
		System.out.println("Indica el tamano del array");
		int tamano = input.nextInt();
		input.nextLine();
		//Creo el array
		String[] cadenas = new String[tamano];
		
		//Pido las cadenas
		for(int i = 0; i < cadenas.length; i++){
			System.out.println("Introduce una cadena");
			cadenas[i] = input.nextLine();
		}
		
		//Indico un desplazamiento, 4 en este caso
		desplazar(cadenas, 2);
		//Resuelto de otra forma
		//cadenas = desplazar2(cadenas, 2);
		
		for(int i = 0; i < cadenas.length; i++){
			System.out.println(cadenas[i]);
			
		}
		
		input.close();
	}

	private static void desplazar(String[] cadenas, int desplazamiento) {
		desplazamiento = desplazamiento % cadenas.length;
		
		//El siguiente bucle se repite tantas veces como el desplazamiento
		for(int i = 0; i < desplazamiento; i++){
			String aux;
			aux = cadenas[cadenas.length - 1];
			//empiezo desde el final del array
			//moviendo el penultimo elemento a la casilla del 1
			for(int j = cadenas.length - 1; j > 0; j--){
				cadenas[j] = cadenas[j - 1];
			}
			cadenas[0] = aux;
		}
	}
	
	private static String[] desplazar2(String[] cadenas, int desplazamiento) {
		//desplazamiento = desplazamiento % cadenas.length;
		String[] cadenas2 = new String[cadenas.length];
		
		//Mientras la casilla del array se desplace a otra mas adelante
		//Simplemente copio el valor
		//Si necesito moverlas al principio del array, calculo su posicion
		for(int i = 0; i < cadenas.length; i++){
			if(desplazamiento + i > cadenas.length -1){
				cadenas2[i + desplazamiento - cadenas.length] = cadenas[i];
			}else{
				cadenas2[i + desplazamiento] = cadenas[i];
			}
		}
		
		return cadenas2;
	}

}

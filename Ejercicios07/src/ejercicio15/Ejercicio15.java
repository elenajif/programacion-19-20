package ejercicio15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce num filas de la matriz");
		int filas =  input.nextInt();
		
		System.out.println("Introduce num columnas de la matriz");
		int columnas =  input.nextInt();
		
		int[][] matriz = new int[filas][columnas];

		//Relleno la matriz con numeros aleatorios
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				matriz[i][j] = (int)(Math.random() * 100) - 50;
			}
		}

		//Muestro la matriz por consola
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		
		ordenar(matriz);
		
		System.out.println("\nMatriz ordenada:");
		//La muestro despues de ordenarla
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
		
		input.close();
	}

	private static void ordenar(int[][] matriz) {
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				
				for(int k = 0; k < matriz.length; k++){
					for(int l = 0; l < matriz[k].length; l++){
						//Si encuentro uno menor que otro
						if(matriz[i][j] < matriz[k][l]){
							int aux = matriz[i][j];
							matriz[i][j] = matriz[k][l];
							matriz[k][l] = aux;
						}
					}
				}
			}
		}
		
	}

}

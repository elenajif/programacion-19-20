package com.elenajif.ejercicios0925;

public class ProfesorTitular extends Profesor {

	int anyosCargo;

	public ProfesorTitular() {

	}

	public ProfesorTitular(String nombre, String apellidos, int edad, String ciclo, int anyosCargo) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosCargo = anyosCargo;
	}

	public int getAnyosCargo() {
		return anyosCargo;
	}

	public void setAnyosCargo(int anyosCargo) {
		this.anyosCargo = anyosCargo;
	}

	public String toString() {
		return "Nombre y apellidos " + nombre + " " + apellidos + "\nEdad " + edad + "\nCiclo " + ciclo
				+ "\nA�os Cargo " + anyosCargo;
	}
	
	public double obtenerSalario() {
		double salario=1100+this.anyosCargo*63.25;
		return salario;
	}

}

package ejercicio02;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ejercicio02 {
	public static void main(String[] args) {
		
		String ruta = "src/ejercicio02/fichero.sql";
		leeFichero(ruta);
			
	}

	private static void leeFichero(String ruta){
		Scanner lector = null;
		try{
			lector = new Scanner(new File(ruta));
			while(lector.hasNextLine()){
				System.out.println(lector.nextLine());
			}
		}catch(FileNotFoundException excepcion){
			//Es lo mismo que hace el metodo printStackTrace()
			System.err.println(excepcion.getMessage());
		}finally{
			if(lector != null){
				lector.close();
			}
		}
	}
	
	
}

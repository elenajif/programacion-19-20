package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		char caracter = pedirCaracter(input);
		System.out.println("El caracter leido es: " + caracter);
		input.close();
	}

	private static char pedirCaracter(Scanner input) {
		String cadena;
		//Pido de nuevo introduce un dato, 
		//mientras no meta un solo caracter
		do{
			System.out.println("Introduce un solo caracter");
			cadena =  input.nextLine();
		}while(cadena.length() != 1);
		
		return cadena.charAt(0);
	}
}

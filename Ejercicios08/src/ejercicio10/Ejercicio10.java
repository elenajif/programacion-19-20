package ejercicio10;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Scanner;

public class Ejercicio10 {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		 
		String[][] baseDatos = cargarDatos();
		
		
			
		int opcion;
		do{
			System.out.println("1 - mostrar estado plazas");
			System.out.println("2 - mostrar plazas alquiladas");
			System.out.println("3 - alquilar plaza");
			System.out.println("4 - eliminar alquiler");
			System.out.println("5 - salir");
			System.out.println("Introduce un num de opcion");
			opcion = input.nextInt();
			input.nextLine();
			
			switch(opcion){
			case 1:
				mostrarPlazas(baseDatos);
				break;
			case 2:
				mostrarPlazasAlquiladas(baseDatos);
				break;
			case 3:
				System.out.println("Introduce el n� de plaza");
				int numPlaza = input.nextInt();
				input.nextLine();
				if(esPlazaLibre(numPlaza -1, baseDatos)){
					System.out.println("Introduce nombre y apellidos");
					String nombre = input.nextLine();
					System.out.println("Introduce matricula");
					String matricula = input.nextLine();
					alquilarPlaza(numPlaza, nombre, matricula, baseDatos);
					
				}else{
					System.out.println("La plaza esta ocupada");
				}
				break;
			case 4:
				System.out.println("Indica la plaza a liberar");
				int plaza = input.nextInt();
				input.nextLine();
				if(!esPlazaLibre(plaza -1, baseDatos)){
					eliminarAlquiler(plaza, baseDatos);
				}else{
					System.out.println("La plaza ya esta libre");
				}
				break;
			case 5:
				guardarDatos(baseDatos);
				System.out.println("Guardando datos");
				break;
			default:
				System.out.println("Opcion no contemplada");
			}
		}while(opcion!= 5);
		input.close();
	}

	private static String[][] cargarDatos() {
		String[][] baseDatos = new String[10][3];
		inicializarBaseDatos(baseDatos);
		Scanner lector = null;
		try {
			
			
			lector = new Scanner(new File("src/ejercicio10/datos.dat"));
			while(lector.hasNextLine()){
				String[] registro = lector.nextLine().split(":");
				int posicion = Integer.parseInt(registro[0]);
				baseDatos[posicion][0] = registro[1];
				baseDatos[posicion][1] = registro[2];
				baseDatos[posicion][2] = registro[3];
			}
		
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}finally{
			if(lector != null){
				lector.close();
			}
		}
		return baseDatos;
	}

	private static void guardarDatos(String[][] baseDatos) {
		PrintWriter escritor = null;
		try {
			escritor = new PrintWriter("src/ejercicio10/datos.dat");
			for(int i = 0; i < baseDatos.length; i++){
				if(!baseDatos[i][0].equals("#libre")){
					String nombre = baseDatos[i][0];
					String matricula = baseDatos[i][1];
					String fecha = baseDatos[i][2];
					escritor.println(i+":"+nombre+":"+matricula+":"+fecha);
				}
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}finally{
			if(escritor !=null){
				escritor.close();
			}
		}
		
	}

	private static void alquilarPlaza(int plaza, String nombre, String matricula, String[][] baseDatos) {
		baseDatos[plaza -1][0] = nombre;
		baseDatos[plaza -1][1] = matricula;
		baseDatos[plaza -1][2] = LocalDate.now().toString();
 	}

	private static void eliminarAlquiler(int plaza, String[][] baseDatos) {
		baseDatos[plaza -1][0] = "#libre";
		
	}

	private static boolean esPlazaLibre(int num, String[][] baseDatos){
		if(baseDatos[num][0].equals("#libre")){
			return true;
		}
		return false;
	}
	
	private static int mostrarPlazasAlquiladas(String[][] baseDatos) {
		int contadorAlquileres = 0;
		for(int i = 0; i < baseDatos.length; i++){
			if(!esPlazaLibre(i, baseDatos)){
				String nombre = baseDatos[i][0];
				String matricula = baseDatos[i][1];
				System.out.print("Plaza "+ (i + 1) + ": ");
				System.out.println(nombre + " - " + matricula);
				contadorAlquileres++;
			}
		}
		return contadorAlquileres;
	}

	private static void mostrarPlazas(String[][] baseDatos) {
		for(int i = 0; i < baseDatos.length; i++){
			if(esPlazaLibre(i, baseDatos)){
				System.out.println("Plaza "+ (i + 1) + ": libre");
			}else{
				System.out.println("Plaza "+ (i + 1) + ": ocupada");
			}
		}
	}
	
	private static void inicializarBaseDatos(String[][] array){
		for(int i = 0; i < array.length; i++){
			array[i][0] = "#libre";
		}
	}
}

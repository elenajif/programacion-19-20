package com.elenajif.principal;

import com.elenajif.ejerciciointerface.InstrumentoViento;
import com.elenajif.ejerciciointerface.Guitarra;
import com.elenajif.ejerciciointerface.Flauta;
import com.elenajif.ejerciciointerface.InstrumentoPercusion;
import com.elenajif.ejerciciointerface.Tambor;

public class Programa {
	public static void main(String[] args) {
		/** Objeto miGuitarra de tipo InstrumentoViento */
		InstrumentoViento miGuitarra = new Guitarra();
		System.out.println(miGuitarra.tipoInstrumento());
		miGuitarra.tocar();
		miGuitarra.afinar();
		/** Objeto miFlauta de tipo InstrumentoViento */
		InstrumentoViento miFlauta = new Flauta();
		System.out.println(miFlauta.tipoInstrumento());
		miFlauta.tocar();
		miFlauta.afinar();
		/** Objeto miTambor de tipo InstrumentoPercusion */
		InstrumentoPercusion miTambor = new Tambor();
		System.out.println(miTambor.tipoInstrumento());
		miTambor.tocar();
		miTambor.afinar();
	}
}

package com.elenajif.ejerciciointerface;

public class InstrumentoPercusion implements InstrumentoMusical {
	public void tocar() {
		System.out.println("Tocar instrumento percusión");
	}

	public void afinar() {
		System.out.println("Afinar instrumento percusión");
	}

	public String tipoInstrumento() {
		return "Instrumento percusión";
	}
}

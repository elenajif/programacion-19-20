package com.elenajif.ejerciciointerface;

public class InstrumentoViento implements InstrumentoMusical {
	public void tocar() {
		System.out.println("Tocar instrumento viento");
	}

	public void afinar() {
		System.out.println("Afinar instrumento viento");
	}

	public String tipoInstrumento() {
		return "Instrumento viento";
	}
}

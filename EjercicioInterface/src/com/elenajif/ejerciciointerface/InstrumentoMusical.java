package com.elenajif.ejerciciointerface;

//Un interfaz es una lista de acciones que puede llevar a cabo un determinado objeto. 
//En una clase adem�s de aparecer los m�todos aparec�a el c�digo para dichos m�todos
//En cambio, en un interfaz s�lo existe el prototipo de una funci�n, no su c�digo.

// Vamos a pensar en un interfaz que en su lista de m�todos sean
// despegar, aterrizar, servirComida y volar. 
// Parece claro que es un avi�n
// Un avi�n engloba las acciones que hemos detallado antes
// A pesar que existan muchos objetos avi�n diferentes entre s�, Boeing 747, Boeing 737
// Todos ellos, aunque sean de clases distintas, poseen el interfaz avi�n
// Es decir, poseen los m�todos detallados en la lista del interfaz avi�n.

// A cualquier avi�n le podemos pedir que vuele, sin importarnos a que clase pertenezca el avi�n
// Cada clase especificar� como volar� el avi�n (c�digo de volar).

// Existen varias diferencias entre una clase abstracta y una interfaz:

// Una clase abstracta puede heredar o extender cualquier clase 
// (independientemente de que esta sea abstracta o no)	
// Una interfaz solamente puede extender o implementar otras interfaces.

// Una clase abstracta puede heredar de una sola clase (abstracta o no) 	
// Una interfaz puede extender varias interfaces de una misma vez.

// Una clase abstracta puede tener m�todos que sean abstractos o que no lo sean	
// Las interfaces s�lo y exclusivamente pueden definir m�todos abstractos.

// En una clase abstracta, los m�todos abstractos pueden ser public o protected. 	
// En una interfaz solamente puede haber m�todos p�blicos.

// Las clases abstractas se usan muy poco. 
// Las interfaces se utilizan mucho cuando trabajas con interfaces gr�ficas.

// Si necesitas programar una clase de la que vayan a heredar otras 
// Pero que esas otras clases que heredan, compartan alguna funcionalidad 
// Ejemplo: clase abstracta -> Persona. Clases que heredan -> Alumno, Profesor... 
// Todas pueden tener un atributo nombre y m�todos get y set de este atributo que hagan lo mismo)
// En ese caso clase abstracta
// En caso contrario, interfaz.

public interface InstrumentoMusical {
	void tocar();
	void afinar();
	String tipoInstrumento();
}

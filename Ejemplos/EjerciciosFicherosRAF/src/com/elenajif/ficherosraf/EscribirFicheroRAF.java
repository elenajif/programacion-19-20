package com.elenajif.ficherosraf;

import java.io.IOException;
import java.io.RandomAccessFile;

// RAF
// Random Access File
// No guarda texto plano, guarda registros
// Permite acceder a una posici�n determinada
// Me puedo desplazar por los registros
// Puedo leer y escribir a la vez
// modo 
// r -> read
// w -> write
// metodos escribir (writeInt, writeLong, writeDouble, writeBytes)
// getFilePointer devuelve la posici�n actual donde se va a realizar la operaci�n
// seek coloca el fichero en una posici�n determinada
// length tama�o archivo

public class EscribirFicheroRAF {

	public static void main(String[] args) {
		try {
			// 1.- Abrir el archivo en acceso RAF
			RandomAccessFile f = new RandomAccessFile("datos.txt", "rw");

			// 2.- nos posicionamos al final del fichero
			f.seek(f.length());

			// 3.- escribimos una cadena de texto
			f.writeBytes("Esto es un texto");

			// 4.- cerramos el fichero
			f.close();
			System.out.println("Fichero actualizado correctamente");
		} catch (IOException e) {
			System.out.println("Error entrada salida");
		}
	}

}

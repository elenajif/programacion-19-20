package com.elenajif.copiarvectorraf;

public class Principal {

	public static void main(String[] args) {
		// crear el vector
		ListaProductos unaLista = new ListaProductos();
		// rellenar el vector
		System.out.println("Rellenamos vector");
		unaLista.rellenarLista();
		System.out.println("Visualizamos vector");
		unaLista.visualizarLista();
		// descargaremos el vector al archivo
		System.out.println("Copiamos información al archivo");
		unaLista.copiarListaAArchivo("datos2");
		System.out.println("Mostrando datos del archivo");
		unaLista.visualizarArchivo("datos2");		
		// modificaremos precios
		System.out.println("Modificando precios");
		unaLista.modificarPrecioArchivo("datos2");
		System.out.println("Mostrando datos del archivo modificado");
		unaLista.visualizarArchivo("datos2");
	}

}

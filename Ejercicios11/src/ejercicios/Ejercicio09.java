package ejercicios;

import java.util.ArrayList;
import java.util.Scanner;

import clases.Director;
import clases.Empleado;
import clases.Oficial;
import clases.Operario;
import clases.Tecnico;

public class Ejercicio09 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		ArrayList<Empleado> lista = new ArrayList<>();
		int opcion;
		
		do{
		System.out.println("Selecciona el tipo de empleadp");
		System.out.println("1 - Empleado");
		System.out.println("2 - Directivo");
		System.out.println("3 - Operario");
		System.out.println("4 - Tecnico");
		System.out.println("5 - Oficial");
		System.out.println("6 - Salir");
		
		opcion = input.nextInt();
		input.nextLine();
		String nombre = null;
		if(opcion != 6){
			System.out.println("Introduce nombre");
			nombre = input.nextLine();
		}
		
		switch(opcion){
			case 1:
				lista.add(new Empleado(nombre));
				break;
			case 2:
				lista.add(new Director(nombre));
				break;
			case 3:
				lista.add(new Operario(nombre));
				break;
			case 4:
				lista.add(new Tecnico(nombre));
				break;
			case 5:
				lista.add(new Oficial(nombre));
				break;
			case 6:
				break;
			
			default:
				System.out.println("Opcion no contemplada");
		}
		
		}while(opcion != 6);
		
		System.out.println("\nAhora listo:");
		for(Empleado empleado : lista){
			System.out.println(empleado);
		}
		
		input.close();
	}

}

package ejercicios;

import clases.Director;
import clases.Empleado;
import clases.Oficial;
import clases.Operario;
import clases.Tecnico;

public class Ejercicio07 {
	public static void main(String[] args){
		
		Empleado empleado = new Empleado("fer");
		Operario operario = new Operario("juan");
		Director director =  new Director("alberto");
		Oficial oficial = new Oficial("Jose");
		Tecnico tecnico = new Tecnico("Laura");
		
		System.out.println(empleado);
		System.out.println(operario);
		System.out.println(director);
		System.out.println(oficial);
		System.out.println(tecnico);
		
		
	}
}

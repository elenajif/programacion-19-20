package clases;

public class Avion extends Vehiculo {
	private int numMisiles;
	
	public Avion(String matricula, String marca, int plazas, int numMisiles){
		super(matricula, marca, plazas);
		this.numMisiles = numMisiles;
	}
	
	public int getNumMisiles() {
		return numMisiles;
	}
	public void setNumMisiles(int numMisiles) {
		this.numMisiles = numMisiles;
	}
	
	public void disparar(){
		if(numMisiles > 0){
			numMisiles--;
		}
	}
	
	//Uso el m�todo toString de la clase Vehiculo
	@Override
	public String toString() {
		return super.toString() + " misiles: " + numMisiles;
	}
}

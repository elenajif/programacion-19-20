package clases;

import java.time.LocalTime;

public class SuperClase {
	private String nombre;

	public SuperClase(String nombre){
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}
	
	public final void muestraHora(){
		System.out.println(LocalTime.now());
	}
	
}

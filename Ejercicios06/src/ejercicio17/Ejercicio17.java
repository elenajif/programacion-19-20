package ejercicio17;

import ejercicio17matematicas.Matematicas;

public class Ejercicio17 {

	public static void main(String[] args) {
		System.out.println(Matematicas.absoluto(5));
		System.out.println(Matematicas.absoluto(-7.5));
		System.out.println(Matematicas.maximo(5, -6));
		System.out.println(Matematicas.maximo(4.346, Math.PI));
		System.out.println(Matematicas.minimo(-6, -8));
		System.out.println(Matematicas.minimo(-5.2345, 5.23456));
		System.out.println(Matematicas.potencia(4, 1));
		System.out.println(Matematicas.aleatorio(70));
		System.out.println(Matematicas.aleatorio(1500, 1560));
		System.out.println(Matematicas.redondear(6.557));
		System.out.println(Matematicas.redondearAlza(6.346));
		System.out.println(Matematicas.redondearBaja(7.4576));

	}

}

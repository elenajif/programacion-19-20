package ejercicio09;

import ejercicio08.Ejercicio08;

public class Ejercicio09 {

	public static void main(String[] args) {
		
		String cadena = palabraAleatoria(700);
		
		System.out.println(cadena);
	}
	
	static String palabraAleatoria(int longitud){
		String cadena = "";
		
		for(int i = 0; i < longitud; i++){
			cadena = cadena + (char)Ejercicio08.aleatorio(65, 90);
		}
		
		return cadena;
	}
}

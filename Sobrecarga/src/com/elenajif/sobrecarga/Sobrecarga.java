package com.elenajif.sobrecarga;

public class Sobrecarga {
	public static void demoSobrecarga() {
		System.out.println("M�todo sobrecargado sin par�metros");
	}
	
	public static void demoSobrecarga(int a) {
		System.out.println("M�todo con un par�metro "+a);
	}
	
	public static int demoSobrecarga(int a, int b) {
		System.out.println("M�todo con dos par�metros "+a +" y "+b);
		return a+b;
	}
	
	public static double demoSobrecarga(double a, double b) {
		System.out.println("M�todo con dos par�metros "+a+" y "+b);
		return a+b;
	}
	
	public static void main (String args[]) {
		System.out.println("M�todo 1");
		Sobrecarga.demoSobrecarga();
		
		System.out.println("M�todo 2");
		Sobrecarga.demoSobrecarga(3);
		
		System.out.println("M�todo 3");
		int x=Sobrecarga.demoSobrecarga(2,3);
		System.out.println(x);
		
		System.out.println("M�todo 4");
		double y=Sobrecarga.demoSobrecarga(4.2,3.4);
		System.out.println(y);
	}
}

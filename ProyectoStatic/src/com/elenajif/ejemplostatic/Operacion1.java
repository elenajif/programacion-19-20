package com.elenajif.ejemplostatic;

import java.util.Scanner;

public class Operacion1 {

	static Scanner in = new Scanner(System.in);
	
	public static int sumar(int x1, int x2) {
		int s = x1 + x2;
		return s;
	}

	public static int restar(int x1, int x2) {
		int r = x1 - x2;
		return r;
	}
	
	public static int multiplicar(int x1, int x2) {
		int m = x1 * x2;
		return m;
	}
	
	public static int dividir(int x1, int x2) {
		int d = x1 / x2;
		return d;
	}
	
	public static int Lectura() {
		System.out.println("Dame un n�mero entero: ");
		int n=in.nextInt();
		return n;
	}

	public static void main(String[] args) {
		int num1= Operacion1.Lectura();
		int num2= Operacion1.Lectura();
		
		System.out.println("La suma de "+num1+" y "+num2+" es:");
		System.out.println(Operacion.sumar(num1, num1));
		System.out.println("La resta de "+num1+" y "+num2+" es:");
		System.out.println(Operacion.restar(num1, num2));
		System.out.println("La multiplicaci�n de "+num1+" y "+num2+" es:");
		System.out.println(Operacion.multiplicar(num1, num2));
		System.out.println("La divisi�n entera de "+num1+" y "+num2+" es:");
		System.out.println(Operacion.dividir(num1, num2));
		in.close();
	}

}

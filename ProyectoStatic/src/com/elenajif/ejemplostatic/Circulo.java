package com.elenajif.ejemplostatic;

import java.util.Scanner;

public class Circulo {
    static final double numeroPi = 3.1416;

    public static double calcularAreaCirculo(double radio){
        double area = Math.pow(radio, 2) * numeroPi;
        return area;
    }
   
    public static double calcularLongitudCirculo(double radio){
        double longitud = 2 * numeroPi * radio;
        return longitud;
    }
    public static void main (String args[]) {
    	Scanner lectura = new Scanner(System.in);
    	System.out.println("Dame el radio:");
    	double r=lectura.nextDouble();
    	System.out.println("El �rea es: "+Circulo.calcularAreaCirculo(r));
    	System.out.println("La longitud es: "+Circulo.calcularLongitudCirculo(r));
    	lectura.close();
    }
}
package com.elenajif.ejemplostatic;

public class Operacion {

	public static int sumar(int x1, int x2) {
		int s = x1 + x2;
		return s;
	}

	public static int restar(int x1, int x2) {
		int r = x1 - x2;
		return r;
	}
	
	public static int multiplicar(int x1, int x2) {
		int m = x1 * x2;
		return m;
	}
	
	public static int dividir(int x1, int x2) {
		int d = x1 / x2;
		return d;
	}

	public static void main(String[] args) {
		System.out.println("La suma de 2 y 4 es:");
		System.out.println(Operacion.sumar(2, 4));
		System.out.println("La resta de 6 y 2 es: ");
		System.out.println(Operacion.restar(2, 2));
		System.out.println("La multiplicación de 8 y 3 es: ");
		System.out.println(Operacion.multiplicar(8, 3));
		System.out.println("La división entera de 7 y 3 es: ");
		System.out.println(Operacion.dividir(7, 3));
	}

}
